import { useContext } from "react";
import Card from "../ui/Card";
import classes from "./MeetupItem.module.css";

import FavoritesContext from "../../store/favorites-context";

function MeetupItem(props) {
  const FavoritesCtx = useContext(FavoritesContext);
  const isFavorite = FavoritesCtx.isItemFavorite(props.id);

  function toggleFavoritesButtonHandler() {
    if (isFavorite) {
      FavoritesCtx.removeFavorite(props.id);
    } else {
      FavoritesCtx.addFavorite({
        id: props.id,
        title: props.title,
        image: props.image,
        address: props.address,
        description: props.description,
      });
    }
  }

  return (
    <li className={classes.item}>
      <Card>
        <div className={classes.image}>
          <img src={props.image} alt={props.title} />
        </div>
        <div className={classes.content}>
          <h3>{props.title}</h3>
          <address>{props.address}</address>
          <p>{props.description}</p>
        </div>
        <div className={classes.actions}>
          <button onClick={toggleFavoritesButtonHandler}>
            {isFavorite ? "Remove from Favorites" : "To Favorites"}
          </button>
        </div>
      </Card>
    </li>
  );
}

export default MeetupItem;
