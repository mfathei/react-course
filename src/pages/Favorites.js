import { useContext } from "react";

import FavoritesContext from "../store/favorites-context";
import MeetupList from "../components/meetups/MeetupList";

function FavoritesPage() {
  const FavoritesCtx = useContext(FavoritesContext);

  let content = <p>You have no Favorites yet</p>;

  if (FavoritesCtx.totalFavoritesCount > 0) {
    content = <MeetupList meetups={FavoritesCtx.favorites} />;
  }

  return (
    <section>
      <h1>My Favorites</h1>
      {content}
    </section>
  );
}

export default FavoritesPage;
