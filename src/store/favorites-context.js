import { createContext, useState } from "react";

const FavoritesContext = createContext({
  favorites: [],
  totalFavoritesCount: 0,
  addFavorite: (meetup) => {},
  removeFavorite: (meetupId) => {},
  isItemFavorite: (meetupId) => {},
});

export function FavoritesContextProvider(props) {
  const [userFavorites, setUserFavorites] = useState([]);

  function addFavoriteHandler(meetup) {
    setUserFavorites((prevUserFavories) => {
      return prevUserFavories.concat(meetup);
    });
  }

  function removeFavoriteHandler(meetupId) {
    setUserFavorites((prevUserFavories) => {
      return prevUserFavories.filter((meetup) => meetupId !== meetup.id);
    });
  }

  function isItemFavoriteHandler(meetupId) {
    return userFavorites.some((meetup) => meetupId === meetup.id);
  }

  const context = {
    favorites: userFavorites,
    totalFavoritesCount: userFavorites.length,
    addFavorite: addFavoriteHandler,
    removeFavorite: removeFavoriteHandler,
    isItemFavorite: isItemFavoriteHandler,
  };

  return (
    <FavoritesContext.Provider value={context}>
      {props.children}
    </FavoritesContext.Provider>
  );
}

export default FavoritesContext;
